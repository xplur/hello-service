package service

import (
	"log"

	"github.com/google/uuid"
	"gitlab.com/xplur/hello-service/model"
	"gitlab.com/xplur/hello-service/repository"
	"gitlab.com/xplur/hello-service/utils"
)

// HelloService is a hello service
type HelloService struct {
	repo repository.Repository
}

// NewHelloService is a constructor for hello service
func NewHelloService(repo repository.Repository) *HelloService {
	return &HelloService{
		repo: repo,
	}
}

// InsertMessage is to insert message
func (hs *HelloService) InsertMessage(message string) error {
	newMessage := &model.Message{
		ID:        uuid.New(),
		Body:      message,
		CreatedAt: utils.GetTime(),
	}

	log.Println("service in")

	err := hs.repo.InsertMessage(newMessage)

	log.Println("service out")

	return err
}

// GetAllMessages is to get all messages
func (hs *HelloService) GetAllMessages() ([]*model.Message, error) {
	log.Println("service insert in")
	messages, err := hs.repo.GetAllMessages()
	if err != nil {
		return nil, err
	}
	log.Println("service insert out")

	return messages, err
}
