test:
	richgo test -count=1 ./... -v -cover

mock: mock-service mock-repository

mock-service:
	@mockgen -source=./service/service.go -destination=./mock/mock_service.go -package=mock

mock-repository:
	@mockgen -source=./repository/repository.go -destination=./mock/mock_repository.go -package=mock

run:
	go run main.go