package mysql

import (
	"database/sql"
	"log"

	"gitlab.com/xplur/hello-service/model"
	"gitlab.com/xplur/hello-service/repository"
)

// MessageRepository is message repository
type MessageRepository struct {
	db *sql.DB
}

// NewMessageRepository is constructor for message repository
func NewMessageRepository(db *sql.DB) repository.Repository {
	return &MessageRepository{
		db: db,
	}
}

// InsertMessage is to insert messsage
func (mr *MessageRepository) InsertMessage(message *model.Message) error {
	query := `INSERT INTO messages (id, body, created_at) VALUES (?, ?, ?)`

	log.Println("INSERTED")

	_, err := mr.db.Exec(query, &message.ID, &message.Body, &message.CreatedAt)

	return err
}

// GetAllMessages is to get all messages
func (mr *MessageRepository) GetAllMessages() ([]*model.Message, error) {

	log.Println("geted")
	res := make([]*model.Message, 0)

	query := `SELECT id, body, created_at FROM messages`

	rows, err := mr.db.Query(query)
	if err != nil {
		return nil, err
	}

	log.Println("geted 1")

	for rows.Next() {
		message := &model.Message{}
		err := rows.Scan(&message.ID, &message.Body, &message.CreatedAt)
		if err != nil {
			return nil, err
		}
		res = append(res, message)
	}

	log.Println("geted 2")

	return res, err
}
