package utils

import "time"

// GetTime is to get time in int64
func GetTime() int64 {
	return time.Now().UnixNano() / 1000000
}
