package service

import (
	"strconv"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/xplur/hello-service/mock"
	"gitlab.com/xplur/hello-service/model"
	"gitlab.com/xplur/hello-service/utils"
)

func TestInsertMessage(t *testing.T) {
	// Init Dependencies
	ctrl := gomock.NewController(t)
	repo := mock.NewMockRepository(ctrl)
	svc := NewHelloService(repo)

	// Mock Data
	message := "testing"

	// Expected Function Call
	repo.EXPECT().InsertMessage(gomock.Any()).
		Times(1).Return(nil)

	// Function Call
	err := svc.InsertMessage(message)
	assert.NoError(t, err)
}

func TestGetAllMessages(t *testing.T) {
	// Init Dependencies
	ctrl := gomock.NewController(t)
	repo := mock.NewMockRepository(ctrl)
	svc := NewHelloService(repo)

	// Mock Data
	messages := make([]*model.Message, 0)
	for i := 1; i <= 3; i++ {
		message := &model.Message{
			ID:        uuid.New(),
			Body:      "body " + strconv.Itoa(i),
			CreatedAt: utils.GetTime(),
		}
		messages = append(messages, message)
	}

	// Expected Function Call
	repo.EXPECT().GetAllMessages().
		Times(1).Return(messages, nil)

	// Function Call
	res, err := svc.GetAllMessages()
	assert.NoError(t, err)
	assert.Equal(t, 3, len(res))
	assert.Equal(t, "body 1", res[0].Body)
}
