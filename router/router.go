package router

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/xplur/hello-service/model"
	"gitlab.com/xplur/hello-service/service"
)

// API :nodoc:
type API struct {
	Service *service.HelloService
	Router  *mux.Router
}

// NewAPI is to create API
func NewAPI(svc *service.HelloService) *API {
	api := API{
		Service: svc,
		Router:  Router(),
	}

	// Route for sending message and get all messages
	api.Router.HandleFunc("/message/{message}", api.insertMessage).Methods("GET")
	api.Router.HandleFunc("/message", api.getAllMessages).Methods("GET")

	return &api
}

// Router :nodoc:
func Router() *mux.Router {
	r := mux.NewRouter()

	return r
}

func (a *API) insertMessage(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	message := params["message"]

	fmt.Println("Input : ", message)

	err := a.Service.InsertMessage(message)
	if err != nil {
		handleError(w, NewErrorNoMessage(400))
		return
	}

	var data struct {
		Data struct {
			Message string `json:"message"`
			Status  string `json:"status"`
		} `json:"data"`
	}

	data.Data.Message = message
	data.Data.Status = "Success"

	handleJSONResponse(w, data)
}

func (a *API) getAllMessages(w http.ResponseWriter, r *http.Request) {
	messages, err := a.Service.GetAllMessages()
	if err != nil {
		log.Println(err)
		handleError(w, NewErrorNoMessage(400))
		return
	}

	var data struct {
		Data struct {
			Length   int             `json:"length"`
			Messages []model.Message `json:"messages"`
		} `json:"data"`
	}

	for _, message := range messages {
		data.Data.Messages = append(data.Data.Messages, *message)
	}

	data.Data.Length = len(messages)

	fmt.Println("Success")

	handleJSONResponse(w, data)
}
