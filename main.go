package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/xplur/hello-service/repository/mysql"
	"gitlab.com/xplur/hello-service/router"
	"gitlab.com/xplur/hello-service/service"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	startServer()
}

func startServer() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8888"
	}

	db := startMySQL()

	repo := mysql.NewMessageRepository(db)

	svc := service.NewHelloService(repo)

	api := router.NewAPI(svc)

	server := &http.Server{
		Handler:      api.Router,
		Addr:         ":" + port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Starting server on port ", port)

	log.Fatal(server.ListenAndServe())
}

func startMySQL() *sql.DB {
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := "root"
	dbName := "db"
	dbAddr := string(os.Getenv("MYSQL_URL"))
	log.Println("Trying to Connect to ", dbAddr)
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbAddr+")/"+dbName)
	if err != nil {
		log.Fatalf("MySQL Problem %s", err.Error())
	}
	log.Println("DB Connected to ", dbAddr)
	return db
}
