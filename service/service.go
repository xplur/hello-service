package service

import "gitlab.com/xplur/hello-service/model"

// Service interface
type Service interface {
	InsertMessage(message string) error
	GetAllMessages() ([]*model.Message, error)
}
