package mysql

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/xplur/hello-service/model"
	"gitlab.com/xplur/hello-service/utils"
)

func TestInsertMessage(t *testing.T) {
	// Init Dependencies
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	repo := NewMessageRepository(db)

	// Mock Data
	id := uuid.New()
	body := "this is body"
	createdAt := utils.GetTime()
	message := &model.Message{
		ID:        id,
		Body:      body,
		CreatedAt: createdAt,
	}

	// Expected Query
	mock.ExpectExec("INSERT INTO messages").
		WithArgs(message.ID, message.Body, message.CreatedAt).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.InsertMessage(message)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestGetAllMessages(t *testing.T) {
	// Init Dependencies
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	repo := NewMessageRepository(db)

	columns := []string{"id", "body", "created_at"}

	// Expected Query
	rows := mock.NewRows(columns).
		AddRow(uuid.New(), "body 1", utils.GetTime()).
		AddRow(uuid.New(), "body 2", utils.GetTime()).
		AddRow(uuid.New(), "body 3", utils.GetTime())

	mock.ExpectQuery("SELECT (.+) FROM messages").
		WillReturnRows(rows)

	messages, err := repo.GetAllMessages()
	assert.NoError(t, err)
	assert.Equal(t, 3, len(messages))
	assert.Equal(t, "body 1", messages[0].Body)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}
