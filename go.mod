module gitlab.com/xplur/hello-service

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.3
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/stretchr/testify v1.5.1
)
