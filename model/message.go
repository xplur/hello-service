package model

import "github.com/google/uuid"

// Message is a struct to contain message
type Message struct {
	ID        uuid.UUID `json:"id"`
	Body      string    `json:"body"`
	CreatedAt int64     `json:"created_at"`
}
