CREATE TABLE IF NOT EXISTS messages (
    id VARCHAR(36) PRIMARY KEY,
    body VARCHAR(999),
    created_at BIGINT
);