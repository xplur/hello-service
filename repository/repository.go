package repository

import "gitlab.com/xplur/hello-service/model"

// Repository interface
type Repository interface {
	InsertMessage(message *model.Message) error
	GetAllMessages() ([]*model.Message, error)
}
